# -*- coding: utf-8 -*-

import unittest
import datetime
from pyramid import testing


class AppmakerTests(unittest.TestCase):
    def _callFUT(self, zodb_root):
        from .models import appmaker
        return appmaker(zodb_root)

    def test_it(self):
        root = {}
        self._callFUT(root)
        self.assertEqual(root['app_root']['Inicjalizacja'].title, 'Inicjalizacja')

class ViewWikiTests(unittest.TestCase):
    def test_it(self):
        from .views import view_blog
        context = testing.DummyResource()
        request = testing.DummyRequest()
        response = view_blog(context, request)
        self.assertEqual(response['pages'], [1])

class BlogModelTests(unittest.TestCase):
    def _getTargetClass(self):
        from .models import Blog
        return Blog

    def _makeOne(self):
        return self._getTargetClass()()

    def test_it(self):
        blog = self._makeOne()
        self.assertEqual(blog.__parent__,None)
        self.assertEqual(blog.__name__,None)