# -*- coding: utf-8 -*-
import datetime

from docutils.core import publish_parts
import re

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from .models import Blog, Post, Comment


@view_config(context='.models.Blog', renderer='templates/mainPageTemplate.pt')
def view_blog(context, request):
    postsList=context.items()
    postsList = sorted(postsList, key=lambda tup: tup[1].created, reverse=True)
    page = 1
    if request.method == 'POST':
        try:
            print request.params['page'],
            page = int(request.params['page'])
        except:
            pass
    posts = list()
    for item in postsList[(page-1)*10:(page*10-1)]:
        try:
            posts.append(item)
        except:
            pass
    num  ={1:1}
    for currentPost in posts:
        num[currentPost]=1
        for comment in currentPost[1].comment:
            num[currentPost]+=1
    numberOfPages = list()
    for i in range (0,len(postsList)/10+1):
        numberOfPages.append(i+1)

    return {'blog':posts, 'addUrl':add_post(context, request),'num':num,'pages':numberOfPages}

@view_config(context='.models.Post', renderer='templates/viewPageTemplate.pt')
def view_post(context, request):
    currentPost = context
    return {'post':currentPost,'addComment':add_comment(context, request)}

@view_config(context='.models.Comment',renderer='templates/commentPageTemplate.pt',)
def view_comment(context,request):
    pass


@view_config(name='add', context='.models.Blog')
def add_post(context, request):
    if request.method == 'POST':
        try:
            post = Post(request.params['title'], request.params['content'], datetime.datetime.now())
            post.__name__=request.params['title']
            post.__context__=context
            context[request.params['title']]=post
        except:
            pass

@view_config(name='comment', context='.models.Post')
def add_comment(context,request):
    if request.method == 'POST':
        currentComment = Comment(1,request.params['comment'])
        context.addComment(currentComment)



