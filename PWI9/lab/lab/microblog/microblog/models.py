# -*- coding: utf-8 -*-
import datetime

from persistent.mapping import PersistentMapping
from persistent import Persistent


class Blog(PersistentMapping):
    """ Klasa dla kontenera """
    __name__ = None
    __parent__ = None



class Post(Persistent):
    """ Klasa dla pojedynczego wpisu """
    def __init__(self, title, content, created):
        self.title = title
        self.content = content
        self.created = created
        self.comment = []

    def __getitem__(self, item):
        return self.comment[item]

    def addComment(self, comment):
        self.comment.append(comment)



class Comment(Persistent):

    def __init__(self,key, content):
        self.key = key
        self.content = content

    def __getitem__(self, item):
        return None


def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        app_root = Blog()

        firstpost = Post('Inicjalizacja','Witajcie na nowym forum',datetime.datetime.now())
        app_root['Inicjalizacja'] = firstpost
        firstpost.__name__ = 'Inicjalizacja'
        firstpost.__parent__ = app_root

        firstcomment = Comment(0,'ProbaKomentarza')
        firstcomment.__name__ = 'ProbaKomentarza'
        firstcomment.__parent__ = firstpost

        zodb_root['app_root'] = app_root
        import transaction
        transaction.commit()
    return zodb_root['app_root']

